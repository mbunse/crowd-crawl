import pandas as pd
from crowdl import Session, Crowd, CONFIG # import from this project

if __name__ == "__main__":
	session = Session()
	df = pd.read_sql(session.query(Crowd).statement, session.bind) # SQL query to DataFrame
	for (target, sdf) in df.groupby('target'):
		print(sdf)
