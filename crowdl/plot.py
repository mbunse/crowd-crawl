import argparse
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from crowdl import Session, Crowd, CONFIG # import from this project
from datetime import datetime

if __name__ == "__main__":
	session = Session()
	targets = [x[0] for x in session.query(Crowd.target).distinct()]
	parser = argparse.ArgumentParser(description="Plot the crowd percentage over the time of the day.")
	parser.add_argument("target", help="The name of the bouldering gym to plot", choices=targets)
	parser.add_argument("-p", "--past-days", help="Only plot the last n days", type=int, default=-1)
	parser.add_argument("-d", "--weekday", help="Only plot the specified day of the week", type=int, default=-1)
	parser.add_argument("-t", "--today", help="Only plot the current day of the week", action="store_true")
	parser.add_argument("-w", "--weekend", help="Only plot days of the weekend", action="store_true")
	parser.add_argument("-n", "--no-weekend", help="Omit all days of the weekend", action="store_true")
	args = parser.parse_args()

	# create a DataFrame from an SQL query; filter on the target
	df = pd.read_sql(
		session.query(Crowd).filter_by(target=args.target).statement,
		session.bind
	)

	# select days of the week
	today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
	if args.weekday > -1:
		df = df[[t.weekday() == args.weekday for t in df['time']]]
		print(f"Selected {len(df.index)} rows on days of the weekday {args.weekday} (zero-based)")
	elif args.today:
		dow = datetime.today().weekday() # current day of the week
		df = df[[t.weekday() == dow for t in df['time']]]
		print(f"Selected {len(df.index)} rows on days of the current weekday, {dow} (zero-based)")
	elif args.weekend:
		df = df[[t.weekday() >= 5 for t in df['time']]]
		print(f"Selected {len(df.index)} rows on the weekend")
	elif args.no_weekend:
		df = df[[t.weekday() < 5 for t in df['time']]]
		print(f"Selected {len(df.index)} rows that are not on the weekend")
	else:
		print(f"Selected {len(df.index)} rows at {args.target}")

	# additional selection of the past n days
	if args.past_days > -1:
		df = df[[(today - t).days < args.past_days for t in df['time']]]
		print(f"Selected {len(df.index)} rows on days of the past {args.past_days} days")

	# prepare groupby (and the color palette)
	df['day'] = df['time'].dt.floor('d') # omit the time of the day to group days
	n_days_past = df['day'].nunique() - 1 if np.any([df['day'] == today]) else df['day'].nunique() + 1

	# prepare the plot (date format and color)
	fig, ax = plt.subplots()
	plt.title(args.target)
	fig.autofmt_xdate()
	ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
	ax.set_prop_cycle(
		color = [plt.cm.YlGnBu(i) for i in np.linspace((2/n_days_past)**2, 1, n_days_past)],
		linewidth = np.geomspace(0.75, 1.25, n_days_past)
	)

	# plot each day separately
	for day, sdf in df.groupby('day'):
		s = pd.Series(
			data=sdf['percent'].values,
			index=[t.replace(1992, 3, 29) for t in sdf['time']]
		) # convert to series (fix 'time' to the same day)

		# only plot opening hours
		if day.weekday() < 5:
			s = s.between_time('11:59', '23:01') # 12:00-23:00 on Mo-Fr
		else:
			s = s.between_time('09:59', '22:01') # 10:00-22:00 on Sa-So

		# plot the crowd percentage over the time of the day
		plot_kwargs = {'color': 'purple', 'linewidth': 2} if day == today else {}
		ax.plot(s.index, s, label=day.strftime("%a, %d.%m."), **plot_kwargs)

	# show the plot
	ax.axhline(.33, color='orange', linewidth=0.75, zorder=-1, linestyle="dashed")
	ax.axhline(.5, color='red', linewidth=0.75, zorder=-1, linestyle="dashed")
	ax.legend()
	plt.show()
