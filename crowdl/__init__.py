import sqlalchemy, os.path, yaml
from sqlalchemy import Column, DateTime, Float, Integer, String
from sqlalchemy.ext.declarative import declarative_base

CROWD_NAME = "crowd" # name of the SQL table

with open("config.yml") as stream:
	CONFIG = yaml.safe_load(stream)

Base = declarative_base()
class Crowd(Base):
	"""Crowd status (time and percent)"""
	__tablename__ = CROWD_NAME

	id = Column(Integer, primary_key=True)
	target = Column(String)
	time = Column(DateTime)
	percent = Column(Float)

	def __repr__(self):
		return f"<Crowd(target={self.target}, time={self.time}, percent={self.percent})>"

def __create_engine(db_path=CONFIG["db"]):
	engine = sqlalchemy.create_engine(f"sqlite:///{db_path}")
	if not engine.has_table(CROWD_NAME):
		print(f"Creating new table {CROWD_NAME} at {db_path}")
		Base.metadata.create_all(engine)
	else:
		print(f"Using the table {CROWD_NAME} at {db_path}")
	return engine

engine = __create_engine() # must be called after Crowd is defined
Session = sqlalchemy.orm.sessionmaker(bind=engine) # Session() creates a session
