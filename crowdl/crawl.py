import pyppdf.patch_pyppeteer, re, traceback
from crowdl import CONFIG, Crowd, mail, Session # import from this project
from datetime import datetime
from requests_html import HTMLSession

TRAFFIC_SIGN_PERCENT = {'green': .1667, 'yellow': 0.5, 'red': 0.8333}

def crawl_crowd(gym_config):
	"""Get the current crowd status at the given gym."""
	print(f"Crawling {gym_config['url']}")
	session = HTMLSession()
	r = session.get(gym_config["url"])
	time = datetime.now()
	return Crowd(
		target = gym_config["id"],
		time = time,
		percent = __crawl_crowd(r.html, gym_config["id"])
	)

def __crawl_crowd(html, gym_id):
	"""Crawl the html from session.get() for the crowd percentage, depending on the gym_id."""
	if gym_id == "gluecksgriff":
		html.render(sleep=3, timeout=15., wait=1., keep_page=True) # arguments only needed on the Raspberry
		bar_style = html.find(".bar", first=True).attrs["style"]
		return float(re.match(r"width: (.*)%;", bar_style)[1]) / 100

	elif gym_id == "boulderwelt":
		html.render(sleep=3, timeout=15., wait=1., keep_page=True) # arguments only needed on the Raspberry
		pointer_text = html.find(".crowd-level-pointer > div", first=True).text
		try:
			return float(re.match(r"(.*)%", pointer_text)[1]) / 100
		except Exception as e:
			print("Using the left-margin fallback. Initial error: {}".format(e))
			arrow_style = html.find(".crowd-level-pointer > img", first=True).attrs["style"]
			left_margin = float(re.match(r"margin-left: (.*)%;", arrow_style)[1]) # fallback solution, expected to be zero
			if left_margin == 0:
				return 0.
			else:
				raise AttributeError("Fallback does not yield the expected 0%. Initial error: {}".format(e))

	# similar websites: https://www.boulderado.eu/referenzen/ (all using the same cashier system)
	elif gym_id in ["monolith", "blocbuster"]:
		taken = float(html.find(".actcounter-content > span", first=True).attrs["data-value"])
		free = float(html.find(".freecounter-content > span", first=True).attrs["data-value"])
		return taken / (taken + free) # taken places / all places

	elif gym_id == "neoliet":
		gyms = [x.text for x in html.find("span")] # which gyms are listed?
		on = [x.attrs["class"][0] for x in html.find(".on")] # which of the lights is on?
		percent = [TRAFFIC_SIGN_PERCENT[x] for x in on] # how much percent does that mean?
		return percent[2] # this is Wattenscheid; TODO crawl the other gyms on that page, too

	else:
		raise AttributeError(f"target='{target}'' is not known")

def open_gyms():
	"""Return the configuration of gyms that are open right now."""
	return list(filter(__is_currently_open, CONFIG["gyms"]))

def __is_currently_open(gym_config):
	"""Return 'true' iff the gym is open right now."""
	now = datetime.now()
	o, c = gym_config['opening_hours'][now.weekday()] # today's opening and closing hours
	return now.replace(hour=o, minute=0) <= now < now.replace(hour=c, minute=0)

if __name__ == "__main__":
	session = Session()
	gyms_to_crawl = open_gyms() # which gyms to crawl depends on their opening hours

	if len(gyms_to_crawl) > 0:
		for gym_config in gyms_to_crawl:
			try:
				session.add(crawl_crowd(gym_config))
			except:
				message = traceback.format_exc()
				print("\n", message)
				mail.send_error_report(CONFIG['outbox'], gym_config["id"], message) # send report via mail
		session.commit()
		print(f"{session.query(Crowd).count()} entries are stored at {datetime.now()}.")

	else:
		print(f"No gyms to crawl at {datetime.now()}.")
