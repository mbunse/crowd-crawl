import getpass, smtplib
from crowdl import CONFIG # import from this project
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from sqlalchemy import desc

# HTML template of the e-mail, in which "{LIST}" will be replaced
MAIL_FORMAT = """
<html style=\"font-family: Arial;\">
  <body>
    <h1>crowd-crawl error report</h1>
    <p>An exception occured while crawling <b>{GYM}</b>.</p>
    <pre>
{REPORT}
    </pre>
  </body>
</html>
"""

def format_mail(gym_id, error_report):
	"""Format an e-mail with a list of vacancies."""
	return MAIL_FORMAT \
	  .replace("{GYM}", gym_id) \
	  .replace("{REPORT}", error_report)

class Outbox:
	"""Auto-closeable e-mail outbox."""
	def __init__(self, config):
		self.config = config
	def __get_key(self):
		try:
			with open(self.config['keyfile'], 'r') as f:
				return f.read().replace('\n', '')
		except Exception as err:
			print(f"The key can not be inferred from the configuration: {err}")
		return getpass.getpass(prompt=f"Password for {self.config['username']}: ")
	def __enter__(self):
		self.server = smtplib.SMTP(self.config['server'], self.config['port'])
		self.server.starttls()
		self.server.login(self.config['username'], self.__get_key())
		return self
	def __exit__(self, type, value, traceback):
		self.server.quit()
	def send(self, text):
		msg = MIMEMultipart()
		msg['From'] = self.config['address']
		msg['To'] = self.config['address']
		msg['Subject'] = "crowd-crawl error report"
		msg.attach(MIMEText(text, 'html'))
		self.server.sendmail(self.config['address'], self.config['address'], msg.as_string())
		print(f"Error report sent to {self.config['address']}")

def send_error_report(outbox_config, gym_id, error_report):
	"""Send a mail with all vacancies to all recipients."""
	with Outbox(outbox_config) as outbox:
		outbox.send(format_mail(gym_id, error_report))
