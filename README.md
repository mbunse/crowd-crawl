# crowdl

Crawl how many people are crowding the Bouldering gyms.

## Setup

The project is set up with virtualenv and pip. On a Raspberry, there are some obstacles that are overcome in the next subsection. If you are crawling with a regular desktop computer, just make sure that `python3.6`, `pip`, and `virtualenv` are installed. You can then skip the next subsection.

### Raspberry Pi Setup

Crowdl's dependencies require `python3.6`, which is not packaged for Raspbian. You can build it yourself, which takes several hours but only needs a few steps. These are well described online: https://medium.com/@isma3il/install-python-3-6-or-3-7-and-pip-on-raspberry-pi-85e657aadb1e

Next, you need to install `virtualenv` for your new python version:

    python3.6 -m pip install --user virtualenv

On my Raspberry, I also had to install some external libraries before I could proceed with the next steps:

    sudo apt install libxml2-dev libxslt-dev chromium-browser

**Note:** You can do the crawling with your Raspberry, but you cannot create the plots with it yet.

### virtualenv Setup

First, create a virtual environment with python 3.6. This will create a directory `venv` in your repository:

    python3.6 -m venv venv

Then, install all dependencies with pip:

    venv/bin/pip install -r requirements.txt

**Note for the Raspberry setup:** The previous command needs to build numpy on the raspberry. This process can take several hours!

**Note for the Raspberry setup:** The javascript interpreter `pyppeteer` unfortunatley installs an incorrect version of chromium when you crawl for the first time. To fix this issue, crawl once (it will fail; see below how it is done) and replace the incorrect version with the correct one:

    pushd ~/.local/share/pyppeteer/local-chromium/<some weird random number>/chrome-linux  # change the directory
    mv chrome chrome.bck  # keep a backup of the original version
    ln -s /usr/bin/chromium-browser chrome  # add a symbolic link to the right version
    popd  # change the directory back to where you have been before

Setup complete! You can now start crawling:

    venv/bin/python -m crowdl.crawl

### cron Setup

To have the crawler run every half-hour during the opening hours, you can configure a cron job which calls the `crowdl.crawl` module at the specified times. From you terminal, run `crontab -e` to edit your cron job list and enter the following line:

    15,45 6-23 * * * cd <path/to/crowd-crawl> && venv/bin/python -m crowdl.crawl >> cron.log 2>&1

## Syncronizing the Database

I wanted to have the latest version of the Raspberry's database file on my personal computer, at any time. I added the following two lines to my crontab *on my personal computer*. The first line is necessary on Ubuntu, because the `~/.ssh/id_rsa` is password-protected with the given keyring on this system.

    SSH_AUTH_SOCK=/run/user/1000/keyring/ssh
    15,35,55 7-22 * * * cd /home/bunse/Repos/crowd-crawl && rsync -vth raspberrypi:/home/pi/Repos/crowd-crawl/db.sql rsync.sql >> rsync.log 2>&1

